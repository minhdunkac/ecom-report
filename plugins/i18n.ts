import { createI18n } from 'vue-i18n'

export default defineNuxtPlugin(({ vueApp }) => {
    const i18n = createI18n({
        legacy: false,
        globalInjection: true,
        locale: 'vn',
        messages: {
            vn: {
                reportTitle: 'Báo cáo',
                access_number: 'Số lượt truy cập',
                vendor_number: 'Số lượng nhà cung cấp',
                new_vendor_total: 'Số lượng nhà cung cấp mới',
                product_total: 'Số lượng sản phẩm',
                new_product_total: 'Số lượng sản phẩm mới',
                order_total: "Số lượng đơn hàng",
                order_total_success: "Số lượng đơn hàng thành công",
                order_total_fail: "Số lượng đơn hàng thất bại",
                transaction_total: "Số lượng giao dịch",
                total_service: "Tổng doanh thu dịch vụ",
                total_service_success: "Tổng doanh thu dịch vụ thành công",
                total_service_fail: "Tổng doanh thu dịch vụ thất bại",
            }
        }
    })

    vueApp.use(i18n)
})